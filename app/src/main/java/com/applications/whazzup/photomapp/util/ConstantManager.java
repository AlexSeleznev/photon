package com.applications.whazzup.photomapp.util;

/**
 * Created by Alex on 28.05.2017.
 */

public class ConstantManager {
    public static final int MAX_CONNECTION_TIMEOUT = 5000;
    public static final int MAX_READ_TIMEOUT = 5000;
    public static final int MAX_WRITE_TIMEOUT = 5000;
}
