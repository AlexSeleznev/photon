package com.applications.whazzup.photomapp.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
public @interface PhotoCardListScope {
}
